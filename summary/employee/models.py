from django.db import models
from datetime import datetime
from simple_history.models import HistoricalRecords

# Create your models here.
class Department(models.Model):

    department_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    timestamp = models.DateTimeField(default=datetime.now())
    history = HistoricalRecords() 

class Employee(models.Model):

    employee_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    username = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    Department = models.ForeignKey(Department, on_delete=models.DO_NOTHING)
    timestamp = models.DateTimeField(default=datetime.now())

